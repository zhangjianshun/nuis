package net.dormforce.nuis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class RentApplication {
    public static void main(String[] args) {
        SpringApplication.run(RentApplication.class,args);
    }
}

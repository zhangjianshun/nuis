# nuis

# NetUnion Information System Rebuild 2017
## 项目简介
* NUIS2017（ NetUnion Information System Rebuild 2017），网络管理委员会内部信息系统
* 技术类型
	* 服务端基于 Java 语言，集成 Spring、Spring Cloud,微服务架构
	* 展示层采用HTML、CSS、JavaScript 并集成第三方库( 如 Materialize )，集成 Vue.js 框架
* 项目特色
	* 采用  Maven  进行自动化构建
	* 集成统一认证中心
*  项目开发环境要求
	* Apache Tomcat
	* MySQL
	* Java
	* Git
	* Maven  
* 模块介绍  

    |模块名称  |  端口 | 简介 |
    | :----------- | :----------- | :----------- |
    |nuis-eureka  | 8761 | 服务注册中心|
    |nuis-train  | 8771 | 培训子系统|
    |nuis-rent  | 8768 | 租借子系统|
    |nuis-repair  | 8769 | 报修子系统|
    |nuis-minute  | 8766 | 纪要子系统|
    |nuis-announcement  | 8762 | 公告子系统|
    |nuis-user  | 8772 | 账户子系统|
    |nuis-auth  | 8763 | 认证服务|
    |nuis-file  | 8764 | 文件服务|
    |nuis-msg  | 8767 | 消息服务|
    |nuis-log  | 8765 | 日志服务|
    |nuis-statistics  | 8770 | 统计服务|
    |nuis-common  | 无 | 公共模块|